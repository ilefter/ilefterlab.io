---
title: Mac下Python的安装和注意事项
date: 2016-07-01 22:19:05
tags:
  - Python
categories: Python
---

Mac OS中自带了Python的2.X版本，但是有Python3.5的需求，而且pip是个棘手的事，搜索引擎提供的方法，都是`easy_install pip`的相关检索，but

![](https://git.oschina.net/iLefter/PublicScreenshots/raw/master/Python/install_error/1.png)

> 众所周知的原因，镇上的墙太高看不到外面的风景。

<!--more-->

这时候需要用到brew，安装Python会自带pip：

`brew install python3`

> 还是因为墙太高，pip模块的安装也不是很顺畅

```bash
Collecting qrcode
  Retrying (Retry(total=4, connect=None, read=None, redirect=None)) after connec
tion broken by 'ReadTimeoutError("HTTPSConnectionPool(host='pypi.python.org', po
rt=443): Read timed out. (read timeout=15)",)': /simple/qrcode/
...
...
raise ReadTimeoutError(self._pool, None, 'Read timed out.')
pip._vendor.requests.packages.urllib3.exceptions.ReadTimeoutError: HTTPSConnecti
onPool(host='pypi.python.org', port=443): Read timed out.
```

于是修改源，使用国内的（方法来自[pip常见问题](https://www.pyant.cn/python/pip-faq.html)）:

临时使用：

`pip install pythonModuleName -i https://pypi.douban.com/simple`


修改了默认的软件源：

修改`/etc/pip.conf` 文件，即可为所有用户配置．
修改`~/.pip/pip.conf`为当前用户配置．


windows下pip配置文件为C:\Users\xx\pip\pip.ini，没有就新建．

```
[global]
index-url = http://pypi.douban.com/simple
trusted-host = pypi.douban.com

```



目前常用源有：

- 清华大学　http://pypi.tuna.tsinghua.edu.cn/simple
- 中国科学技术大学　https://pypi.mirrors.ustc.edu.cn/simple
