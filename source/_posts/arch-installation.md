---
title: Arch的安装以及部分问题的解决
date: 2017-01-24 17:35:01
tags: 
  - Linux
  - Arch
categories: Linux
---

最近在机子上又倒腾了Arch，记录一下我的心路历程

<!--more-->

## Installation

安装的步骤都是基本按照官方的wiki走的，其中WiFi不可用，以太网可正常工作。WiFi的配置后面再说，目前安装阶段以太网就可以了。

主要这机子目前由Clover引导的，UEFI+GPT的方案，三块硬盘，其中两块SSD分别是Windows 10和Mac OS且分别有各自的EFI引导分区，所以在安装前先查询了部分先例再开始安装的。而且最近SSD开始疯狂涨价，本来容量就不大，所以安装在了机械硬盘。

我的分区结构：

```bash
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 931.5G  0 disk 
├─sda1   8:1    0 146.5G  0 part
├─sda2   8:2    0 195.3G  0 part 
├─sda3   8:3    0 195.3G  0 part 
├─sda4   8:4    0   251G  0 part 
├─sda5   8:5    0  19.5G  0 part /
├─sda6   8:6    0   7.8G  0 part [SWAP]
├─sda7   8:7    0  14.7G  0 part /home
├─sda8   8:8    0 101.1G  0 part 
└─sda9   8:9    0   248M  0 part 
sdb      8:16   0 119.2G  0 disk 
├─sdb1   8:17   0   200M  0 part /boot/efi
├─sdb2   8:18   0   128M  0 part 
├─sdb3   8:19   0 118.4G  0 part 
└─sdb4   8:20   0   503M  0 part 
sdc      8:32   0 119.2G  0 disk 
├─sdc1   8:33   0   200M  0 part 
├─sdc2   8:34   0 118.5G  0 part 
└─sdc3   8:35   0 619.9M  0 part
```

如图所示，1T的机械分了三个区根目录、SWAP分区和HOME目录
所以格式化分区：
`▶mkfs.ext4 /dev/sda5`
`▶mkfs.ext4 /dev/sda7`
`▶mkswap /dev/sda6`
然后挂载：
`▶mount /dev/sda5 /mnt`
`▶mkdir -p /mnt/boot/efi`
`▶mount /dev/sdb1 /mnt/boot/efi`
`▶mkdir -p /mnt/home`
`▶mount /dev/sda7 /mnt/home`
再激活SWAP分区
`▶swapon /dev/sda6`


中间就是按照wiki一步一步走，最后比较关键，就是引导的安装，因为是UEFI，所以有：
`▶pacman -S dosfstools grub efibootmgr`
`▶grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Arch --recheck`
`▶grub-mkconfig -o /boot/grub/grub.cfg`

最后重启就好了

## ISSUES
开机发现WiFi还是不能用，我的网卡是当初倒腾黑苹果换的BCM94352HMB的网卡，虽然PCI的信息里能看到BCM4352的信息，但无法设置Wifi，怀疑驱动有问题，根据<https://wireless.wiki.kernel.org/en/users/Drivers/b43#list_of_hardware>所列出的设备和驱动对应关系可知道是[broadcom-wl](https://aur.archlinux.org/packages/broadcom-wl/)驱动的。
而且标准的无效，得用动态内核加载的版本[broadcom-wl-dkms](https://aur.archlinux.org/packages/broadcom-wl-dkms/)，则安装dkms的版本。中间会提示错误，只要更新下内核的编译头文件就好了，命令如下：
`▶sudo pacman -S linux-header`

最后就是双显卡的[Optimus](https://wiki.archlinux.org/index.php/NVIDIA_Optimus)，为了更好的体验就是采用[Bumblebee](https://wiki.archlinux.org/index.php/Bumblebee)，最后运行optirun会提示下面的错误，但是在root用户下却不会，怀疑还是权限的问题。
```bash
Polygons in scene: 62464 (61 spheres * 1024 polys/spheres)
No protocol specified
ERROR in line 614:
Could not open display
```
最终发现是远程桌面协议的问题，运行`▶xhost +`就可以了。

## SHADOWSOCKSR
使用python版本的就可以了，客户端只需运行shadowsocks下的local.py就可以了。
基于*python2*的gfwlist2pac生成pac格式的文件，然后系统设置为代理为本地的pac文件，最后配合SwitchyOmega来实现浏览器的代理。然而目前还不清楚如何设置为全局代理，部分终端下的操作还是被酱（qiang）。
然后配置了systemctl的相关信息，设置为开机启动。运行状态如图：
```bash
● ssr.service - ShadowsocksR-Python
   Loaded: loaded (/usr/lib/systemd/system/ssr.service; enabled; vendor preset
   Active: active (running) since Sun 2017-01-23 15:50:19 CST; 9h ago
 Main PID: 397 (python)
    Tasks: 1 (limit: 4915)
   CGroup: /system.slice/ssr.service
           └─397 /usr/bin/python /usr/local/shadowsocksr/shadowsocks/local.py
```



参考：
 [Arch Linux Wiki](https://wiki.archlinux.org)
 [贴吧：Win10+Arch UEFI双系统安装向](http://tieba.baidu.com/p/4925663894)
 [Arch Linux 中文社区：Arch Linux 安装指南](http://bbs.archlinuxcn.org/viewtopic.php?id=1037)