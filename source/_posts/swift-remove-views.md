---
title: Swift移除子元素或子视图
date: 2016-4-15 21:03:05
tags:
  - Swift
categories: Swift
---

Swift-代码，移除所有子类视图或元素

```swift
func clearViews() {
    for view in self.view.subviews as [UIView] {
        view.removeFromSuperview()
    }
}
```

<!--more-->

通过遍历来移除，但是如果在UIScollView以及其的扩展类UITableView或者UICollectionView的Cell中对所有元素进行这个操作，反复加载和重用Cell后，移除和生成UI元素的代价也就很高了，所以记此以待日后找到更好的方法。
