---
title: MySQL删除重复的行(多字段，无主键)
date: 2017-03-01 17:56:00
tags:
  - MySQL
categories: MySQL
---

关于上个学期的SRTP项目，也开始着手写了

<!--more-->

爬了两天的数据，今天才发现，原来城市空气质量发布平台的更新是一个小时一次，上次服务器挂的任务是半个小时一次。再加上有的站点的数据未及时更新，所以导致数据重复。

原始爬取得数据：

![](https://git.oschina.net/iLefter/PublicScreenshots/raw/master/Hexo-Mysql/1-180512@2x.png)

没办法，对数据库不是很熟悉，MySQL的用法也是迷得不行。

所以查找了一种比较笨的方法:

* 1.我的表结构：

![](https://git.oschina.net/iLefter/PublicScreenshots/raw/master/Hexo-Mysql/3-182519@2x.png)

* 2.直接复制一个表，查出不重复的记录：

```bash
mysql> create table table_201702_copy as (select distinct siteID,siteName,time,aqi,pm2_5,pm10,so2,no2,co,o3,primary_pollution from table_201702);
```

* 3.得到了新表，不含重复数据

![](https://git.oschina.net/iLefter/PublicScreenshots/raw/master/Hexo-Mysql/2-181320@2x.png)



Refre To：[mysql删除无主键表中重复记录(只保留一条记录)](http://blog.csdn.net/feinifi/article/details/52277492)
