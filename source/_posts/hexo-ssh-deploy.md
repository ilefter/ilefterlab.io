---
title: ssh默认端口改变后的解决方法
date: 2015-10-21 22:34:51
tags:
  - ssh
  - git
category: Hexo
---

一般有两种解决方法：

<!--more-->

一、直接在ssh的域名后加上端口号

例如 `ssh://git@ilefter.com:1234/hexo.git`

二、配置本地文件

```json
host domain
hostname domain.com
port 1234
```

直接保存为~/.ssh/config

然后git照常使用即可
